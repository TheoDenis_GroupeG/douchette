﻿using System;
using System.Data.SQLite;

namespace Douchette
{
    class Program
    {

        private static bool TestItemType(string string_to_test)
        {
            return (string_to_test.Length == 3);
        }

        private static bool TestItemYear(int year_to_test)
        {
            String sDate = DateTime.Now.ToString();
            DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

            int yy = Int32.Parse(datevalue.Year.ToString());
            return (yy <= year_to_test && year_to_test > yy - 100);
        }

        private static bool TestItemReference(int reference_to_test)
        {
            return (reference_to_test >= 0 && reference_to_test <= 9999);
        }

        public static bool VerifyCode(string type, int year, int reference)
        {
            return (TestItemType(type) && TestItemYear(year) && TestItemReference(reference));
        }

        private static string GetType(string barcode_as_table)
        {
            string[] barcode_as_table = barcode.split('-');

            return barcode_as_table[0];
        }

        private static string GetYear(string barcode_as_table)
        {
            string[] barcode_as_table = barcode.split('-');

            return Int32.tryParse(barcode_as_table[1]);
        }

        private static string GetReference(string barcode)
        {
            string[] barcode_as_table = barcode.split('-');

            return Int32.tryParse(barcode_as_table[2]);
        }

        public static bool TestBarCode(string barcode)
        {
            string type = GetType(barcode);
            int year = GetYear(barcode);
            int reference = GetReference(barcode);

            //Gets interrupted if Bar Code is not Valid
            //Still works even if there are more than 3 fields, though
            functionX(VerifyCode(type, year, reference));

            return true;
        }

        private static void functionX(bool test)
        {
            if (!test)
            {
                Console.WriteLine("BIP BIP");
                Application.Restart();
            }
        }

        static void Main(string[] args)
        {
            try
            {
                //Building Connexion
                //string path = "db.sqlite3";
                //string cs = "Data Source = :memory:";
                string command = "CREATE TABLE IF NOT EXISTS Items(ID PRIMARY KEY, Type TEXT, Year INT, Reference INT)";
                var con = new SQLiteConnexion(cs);
                con.Open();
                var cmd = new SQLiteCommand(command, con);

                //BarCode Verification
                string barcode = Console.ReadLine(); //1 and 0 to test 
                if (TestBarCode(barcode))
                {
                    //DB Insertion
                    command = "INSERT INTO Tables (Type, Year, Reference) VALUES ( " + GetType(barcode) + ", " + GetYear(barcode) + ", " + GetReference(barcode) + ")";
                    var cmd = new SQLiteCommand(command, con);
                }
            }
            catch
            {
                functionX(false);
            }
        }
    }
}
